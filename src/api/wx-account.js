import request from '@/utils/request'

export function fetchList(data) {
  return request({
    url: '/api/wx/account/list',
    method: 'post',
    params: data
  })
}

export function createRow(data) {
  return request({
    url: '/api/wx/account/add',
    method: 'post',
    data
  })
}

export function updateRow(data) {
  return request({
    url: '/api/wx/account/update',
    method: 'post',
    data
  })
}
export function deleteRow(data) {
  return request({
    url: '/api/wx/account/delete',
    method: 'post',
    params: data
  })
}

export function fetchAllAccount() {
  return request({
    url: '/api/wx/account/listAll',
    method: 'post'
  })
}

export function generateQRUrl(data) {
  return request({
    url: '/api/wx/account/generateQRUrl',
    method: 'post',
    data
  })
}

export function listTreeWxAccount() {
  return request({
    url: '/api/wx/account/listTreeWxAccount',
    method: 'post'
  })
}
